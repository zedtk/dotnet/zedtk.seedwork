using System;
using System.Text.Json;
using Xunit;
using ZeDtk.Seedwork.Serialization.Json.Converters;

namespace ZeDtk.Seedwork.Serialization.Json.Tests
{
    public class NullableIsoDateJsonConverterTests
    {
        #region Public Methods

        [Fact]
        public void Read()
        {
            JsonSerializerOptions options = new();
            options.Converters.Add(new NullableIsoDateJsonConverter());

            DateTime? expected = new(2021, 1, 1);
            DateTime? actual = JsonSerializer.Deserialize<DateTime?>("\"2021-01-01\"", options);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ReadNull()
        {
            JsonSerializerOptions options = new();
            options.Converters.Add(new NullableIsoDateJsonConverter());

            DateTime? expected = null;
            DateTime? actual = JsonSerializer.Deserialize<DateTime?>("null", options);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Write()
        {
            JsonSerializerOptions options = new();
            options.Converters.Add(new NullableIsoDateJsonConverter());

            string expected = "\"2021-01-01\"";
            string actual = JsonSerializer.Serialize<DateTime?>(new DateTime(2021, 1, 1), options);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WriteNull()
        {
            JsonSerializerOptions options = new();
            options.Converters.Add(new NullableIsoDateJsonConverter());

            string expected = "null";
            string actual = JsonSerializer.Serialize<DateTime?>(null, options);
            Assert.Equal(expected, actual);
        }

        #endregion Public Methods
    }
}
