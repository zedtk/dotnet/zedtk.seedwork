using System;
using System.Text.Json;
using Xunit;
using ZeDtk.Seedwork.Serialization.Json.Converters;

namespace ZeDtk.Seedwork.Serialization.Json.Tests
{
    public class AbstractNullableDateTimeJsonConverterTests
    {
        #region Public Methods

        [Fact]
        public void Read()
        {
            JsonSerializerOptions options = new();
            options.Converters.Add(new ConcreteNullableDateTimeJsonConverter());

            DateTime? expected = new(2021, 1, 1);
            DateTime? actual = JsonSerializer.Deserialize<DateTime?>("\"20210101\"", options);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ReadNull()
        {
            JsonSerializerOptions options = new();
            options.Converters.Add(new ConcreteNullableDateTimeJsonConverter());

            DateTime? expected = null;
            DateTime? actual = JsonSerializer.Deserialize<DateTime?>("null", options);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Write()
        {
            JsonSerializerOptions options = new();
            options.Converters.Add(new ConcreteNullableDateTimeJsonConverter());

            string expected = "\"20210101\"";
            string actual = JsonSerializer.Serialize<DateTime?>(new DateTime(2021, 1, 1), options);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WriteNull()
        {
            JsonSerializerOptions options = new();
            options.Converters.Add(new ConcreteNullableDateTimeJsonConverter());

            string expected = "null";
            string actual = JsonSerializer.Serialize<DateTime?>(null, options);
            Assert.Equal(expected, actual);
        }

        #endregion Public Methods

        #region Private Classes

        private class ConcreteNullableDateTimeJsonConverter : AbstractNullableDateTimeJsonConverter
        {
            #region Protected Properties

            protected override string Format => "yyyyMMdd";

            #endregion Protected Properties
        }

        #endregion Private Classes
    }
}
