using System;
using System.Text.Json;
using Xunit;
using ZeDtk.Seedwork.Serialization.Json.Converters;

namespace ZeDtk.Seedwork.Serialization.Json.Tests
{
    public class IsoDateJsonConverterTests
    {
        #region Public Methods

        [Fact]
        public void Read()
        {
            JsonSerializerOptions options = new();
            options.Converters.Add(new IsoDateJsonConverter());

            DateTime expected = new(2021, 1, 1);
            DateTime actual = JsonSerializer.Deserialize<DateTime>("\"2021-01-01\"", options);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Write()
        {
            JsonSerializerOptions options = new();
            options.Converters.Add(new IsoDateJsonConverter());

            string expected = "\"2021-01-01\"";
            string actual = JsonSerializer.Serialize(new DateTime(2021, 1, 1), options);
            Assert.Equal(expected, actual);
        }

        #endregion Public Methods
    }
}
