using System;

namespace ZeDtk.Seedwork.Serialization.Json.Converters
{
    /// <summary>
    /// Converter for nullable <see cref="DateTime"/>.
    /// <see cref="AbstractDateTimeJsonConverter.Format"/> is "yyyy-MM-ddTHH:mm:ss".
    /// </summary>
    public class NullableIsoDateTimeJsonConverter : AbstractNullableDateTimeJsonConverter
    {
        #region Protected Properties

        protected override string Format => "yyyy-MM-ddTHH:mm:ss";

        #endregion Protected Properties
    }
}
