using System;

namespace ZeDtk.Seedwork.Serialization.Json.Converters
{
    /// <summary>
    /// Converter for <see cref="DateTime"/>.
    /// <see cref="AbstractDateTimeJsonConverter.Format"/> is "yyyy-MM-dd".
    /// </summary>
    public class IsoDateJsonConverter : AbstractDateTimeJsonConverter
    {
        #region Protected Properties

        protected override string Format => "yyyy-MM-dd";

        #endregion Protected Properties
    }
}
