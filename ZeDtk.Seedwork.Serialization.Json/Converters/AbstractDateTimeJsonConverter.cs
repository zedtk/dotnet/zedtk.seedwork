using System;
using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ZeDtk.Seedwork.Serialization.Json.Converters
{
    /// <summary>
    /// Abstract converter for <see cref="DateTime"/>.
    /// The <see cref="Format"/> must be specified by the concrete converter.
    /// The <see cref="Culture"/> can be overriden (defaults to <see cref="CultureInfo.InvariantCulture"/>).
    /// The Read operation uses the <see cref="DateTime.ParseExact(string, string, IFormatProvider?)"/> method.
    /// </summary>
    public abstract class AbstractDateTimeJsonConverter : JsonConverter<DateTime>
    {
        #region Protected Properties

        /// <summary>
        /// The culture used for serializing and deserializing dates.
        /// </summary>
        protected virtual CultureInfo Culture { get; } = CultureInfo.InvariantCulture;

        /// <summary>
        /// The format used for serializing and deserializing dates.
        /// </summary>
        protected abstract string Format { get; }

        #endregion Protected Properties

        #region Public Methods

        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return DateTime.ParseExact(reader.GetString(), Format, Culture);
        }

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToString(Format, Culture));
        }

        #endregion Public Methods
    }
}
