using System;

namespace ZeDtk.Seedwork.Serialization.Json.Converters
{
    /// <summary>
    /// Converter for nullable <see cref="DateTime"/>.
    /// <see cref="AbstractDateTimeJsonConverter.Format"/> is "yyyy-MM-dd".
    /// </summary>
    public class NullableIsoDateJsonConverter : AbstractNullableDateTimeJsonConverter
    {
        #region Protected Properties

        protected override string Format => "yyyy-MM-dd";

        #endregion Protected Properties
    }
}
